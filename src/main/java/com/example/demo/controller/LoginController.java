package com.example.demo.controller;

import com.example.demo.exeption.NotFoundException;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(("/"))
public class LoginController {

    final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public Iterable<User> getAll() {
        return userService.findAll();
    }

    @PostMapping("/login")
    public User get(@PathVariable Long id) {
        return userService.findById(id).orElseThrow(NotFoundException::new);
    }

    @PostMapping("/register")
    public User create(@RequestBody User user) {
        return userService.save(new User(user.getUsername(), user.getPassword(), user.getEmail()));
    }
}
