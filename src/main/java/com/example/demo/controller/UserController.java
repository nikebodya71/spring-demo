package com.example.demo.controller;

import com.example.demo.exeption.NotFoundException;
import com.example.demo.exeption.NotSupportException;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(("/user"))
public class UserController {

    final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public Iterable<User> getAll() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public User get(@PathVariable Long id) {
        return userService.findById(id).orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public User create(@RequestBody User user) {
        return userService.save(user);
    }

    @PostMapping("/{id}")
    public User update(@PathVariable Long id, @RequestBody User User) {
        throw new NotSupportException();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        userService.delete(id);
    }

}
