package com.example.demo.controller;

import com.example.demo.exeption.NotFoundException;
import com.example.demo.model.Message;
import com.example.demo.model.User;
import com.example.demo.repository.MessageRepository;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(("/message"))
public class MessageController {

    final MessageRepository messageRepository;

    public MessageController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @GetMapping
    public Iterable<Message> getAll() {
        return messageRepository.findAll();
    }

    @GetMapping("/{id}")
    public Message get(@PathVariable Long id) {
        return messageRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public Message create(@AuthenticationPrincipal User user, @RequestBody Message message) {
        return messageRepository.save(new Message(message.getText(), user));
    }

    @PostMapping("/{id}")
    public Message update(@PathVariable Long id, @RequestBody Message message) {
        return messageRepository.findById(id).map(it -> {
            it.setText(message.getText());
            messageRepository.save(it);
            return it;
        }).orElseThrow(NotFoundException::new);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        if (messageRepository.existsById(id)) {
            messageRepository.deleteById(id);
        } else {
            throw new NotFoundException();
        }
    }

}
